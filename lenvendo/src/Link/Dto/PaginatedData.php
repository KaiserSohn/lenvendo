<?php

declare(strict_types=1);

namespace App\Link\Dto;

class PaginatedData
{
    /**
     * @var int
     */
    private int $totalCount;

    /**
     * @var float
     */
    private float $totalPages;

    /**
     * @var int
     */
    private int $currentPage;

    /**
     * @var array
     */
    private array $items;

    /**
     * PaginatedData constructor.
     * @param int $totalCount
     * @param float $totalPages
     * @param int $currentPage
     * @param array $items
     */
    public function __construct(int $totalCount, float $totalPages, int $currentPage, array $items)
    {
        $this->totalCount = $totalCount;
        $this->totalPages = $totalPages;
        $this->currentPage = $currentPage;
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @return float
     */
    public function getTotalPages(): float
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
