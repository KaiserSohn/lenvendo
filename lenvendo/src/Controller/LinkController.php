<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Link;
use App\Exception\IncorrectPasswordException;
use App\Exception\LinkProviderException;
use App\Exception\NotUniqueUrlException;
use App\Form\LinkDeleteType;
use App\Form\LinkSaveType;
use App\Link\LinkExporter;
use App\Link\LinkProvider;
use App\Repository\LinkRepository;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class LinkController extends AbstractController
{
    /**
     * @var LinkProvider
     */
    private LinkProvider $linkProvider;

    /**
     * @var LinkRepository
     */
    private LinkRepository $linkRepository;

    /**
     * @var LinkExporter
     */
    private LinkExporter $linkExporter;

    /**
     * LinkController constructor.
     * @param LinkProvider $linkProvider
     * @param LinkRepository $linkRepository
     * @param LinkExporter $linkExporter
     */
    public function __construct(LinkProvider $linkProvider, LinkRepository $linkRepository, LinkExporter $linkExporter)
    {
        $this->linkProvider = $linkProvider;
        $this->linkRepository = $linkRepository;
        $this->linkExporter = $linkExporter;
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function export(): Response
    {
        return new BinaryFileResponse($this->linkExporter->export());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $page = (int)$request->get('nav_page');
        $sortBy = $request->get('sort_by');
        $sortDirection = $request->get('sort_direction');
        $data = $this->linkRepository->getLinksWithPagination(
            ($page <= 0) ? 1 : $page,
            $sortBy,
            $sortDirection
        );

        return $this->render('page/index.html.twig', [
            'total_count'  => $data->getTotalCount(),
            'total_pages' => $data->getTotalPages(),
            'page' => $data->getCurrentPage(),
            'items' => $data->getItems(),
            'sort_by' => $sortBy ?? 'id',
            'next_sort_direction' => $sortDirection === 'desc' ? 'asc' : 'desc',
            'sort_direction' => $sortDirection,
        ]);
    }

    /**
     * @param Request $request
     * @param Link $link
     * @return Response
     */
    public function delete(Request $request, Link $link): Response
    {
        if ($link->getPassword()) {
            $form = $this->createForm(LinkDeleteType::class);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $submittedForm = $form->getData();

                try {
                    if (md5($submittedForm['password']) === $link->getPassword()) {
                        $this->linkRepository->remove($link);
                        $this->linkRepository->flush();

                        return $this->redirectToRoute('index');
                    } else {
                        throw new IncorrectPasswordException();
                    }
                } catch (IncorrectPasswordException $incorrectPasswordException) {
                    $form->addError(new FormError('Неправильный пароль'));
                } catch (Throwable $throwable) {
                    $form->addError(new FormError('Произошла ошибка удаления'));
                }
            }

            return $this->render('page/delete_link.html.twig', [
                'form' => $form->createView()
            ]);
        } else {
            try {
                $this->linkRepository->remove($link);
                $this->linkRepository->flush();
            } catch (Throwable $throwable) {

            }
            return $this->redirectToRoute('index');
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function detail(Request $request): Response
    {
        $id = $request->attributes->getInt('id');

        return $this->render('page/detail_link.html.twig', [
            'item' => $this->linkRepository->findOneBy(['id' => $id])
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $link = new Link();

        $form = $this->createForm(LinkSaveType::class, $link);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Link $submittedLink */
            $submittedLink = $form->getData();

            try {
                /** @var Link $existedLink */
                $existedLink = $this->linkRepository->findOneBy(['url' => $submittedLink->getUrl()]);
                $password = $submittedLink->getPassword();

                if ($existedLink) {
                    throw new NotUniqueUrlException();
                }

                $parsedLink = $this->linkProvider->getParsedData($submittedLink->getUrl());

                $submittedLink->setTitle($parsedLink->getTitle());
                $submittedLink->setFavicon($parsedLink->getFavicon());
                $submittedLink->setMetaDescription($parsedLink->getMetaDescription());
                $submittedLink->setMetaKeywords($parsedLink->getMetaKeywords());

                if (isset($password)) {
                    $submittedLink->setPassword(md5($submittedLink->getPassword()));
                }

                $this->linkRepository->persist($submittedLink);
                $this->linkRepository->flush();

                return $this->redirectToRoute('index');
            } catch (LinkProviderException $linkProviderException) {
                $form->addError(new FormError('Ресурс недоступен. Пожалуйста, попробуйте сохранить позже'));
            } catch (NotUniqueUrlException $notUniqueUrlException) {
                $form->addError(new FormError('Данный URL уже добавлен в таблицу'));
            } catch (Throwable $exception) {
                $form->addError(new FormError('Произошла ошибка сохранения.'));
            }
        }

        return $this->render('page/save_link.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
