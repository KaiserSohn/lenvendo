<?php

declare(strict_types=1);

namespace App\Link\Dto;

class ParsedLinkData
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private ?string $favicon;

    /**
     * @var string
     */
    private ?string $metaDescription;

    /**
     * @var string
     */
    private ?string $metaKeywords;

    /**
     * ParsedLinkData constructor.
     * @param string $title
     * @param string $favicon
     * @param string|null $metaDescription
     * @param string|null $metaKeywords
     */
    public function __construct(string $title, ?string $favicon, ?string $metaDescription, ?string $metaKeywords)
    {
        $this->title = $title;
        $this->favicon = $favicon;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getFavicon(): ?string
    {
        return $this->favicon;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @return string|null
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }
}
