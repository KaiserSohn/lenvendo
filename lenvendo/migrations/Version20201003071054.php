<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201003071054 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add table links';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS links (
            id SERIAL PRIMARY KEY NOT NULL,
            title TEXT NOT NULL,
            favicon TEXT NULL,
            url TEXT UNIQUE NOT NULL,
            meta_description TEXT NULL,
            meta_keywords TEXT NULL,
            password TEXT NULL,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        )');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS links');
    }
}
