<?php

declare(strict_types=1);

namespace App\Link;

use App\Entity\Link;
use App\Repository\LinkRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Filesystem\Filesystem;

class LinkExporter
{
    private const FILE_STORAGE_PREFIX = '/tmp/excel_export/';

    private const EXPORT_HEADERS = [
        'id',
        'Заголовок страницы',
        'URL',
        'Meta Description',
        'Meta Keywords',
        'Дата создания',
    ];

    /**
     * @var LinkRepository
     */
    private LinkRepository $linkRepository;

    /**
     * @var Filesystem
     */
    private Filesystem $fileSystem;

    /**
     * @var string
     */
    private string $filePath;

    /**
     * Export constructor.
     * @param LinkRepository $linkRepository
     * @param Filesystem $fileSystem
     */
    public function __construct(LinkRepository $linkRepository, Filesystem $fileSystem)
    {
        $this->linkRepository = $linkRepository;
        $this->fileSystem = $fileSystem;
        $this->fileSystem->mkdir(static::FILE_STORAGE_PREFIX);
    }

    /**
     * @return Link[]|array
     */
    private function getData()
    {
        $dataToExport = $this->linkRepository->findAll();

        return array_map(function (Link $link) {
            return [
                $link->getId(),
                $link->getTitle(),
                $link->getUrl(),
                $link->getMetaDescription(),
                $link->getMetaKeywords(),
                $link->getCreatedAt()
            ];
        }, $dataToExport);
    }

    /**
     * @throws Exception
     */
    public function export()
    {
        $this->filePath = sprintf('%s%s.xlsx', static::FILE_STORAGE_PREFIX, md5(date('d_m_Y_H_i_s')));

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray(array_merge([static::EXPORT_HEADERS], $this->getData()));

        $writer = new Xlsx($spreadsheet);
        $writer->save($this->filePath);

        return $this->filePath;
    }

    public function __destruct()
    {
        $this->fileSystem->remove($this->filePath);
    }
}
