<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Link;
use App\Link\Dto\PaginatedData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

class LinkRepository extends ServiceEntityRepository
{
    private const PAGINATION_LIMIT = 5;

    /**
     * LinkRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    /**
     * @param Link $link
     * @throws ORMException
     */
    public function persist(Link $link)
    {
        $this->getEntityManager()->persist($link);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param Link $link
     * @throws ORMException
     */
    public function remove(Link $link)
    {
        $this->getEntityManager()->remove($link);
    }

    /**
     * @param int $page
     * @param string|null $sortBy
     * @param string|null $sortDirection
     * @return PaginatedData
     */
    public function getLinksWithPagination(int $page, ?string $sortBy, ?string $sortDirection)
    {
        $offset = $page === 1 ? 0 : (($page - 1) * static::PAGINATION_LIMIT);
        $sortDirection = $sortDirection === 'desc' ? 'desc' : 'asc';

        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('link')
            ->from('App\Entity\Link', 'link')
            ->setFirstResult($offset)
            ->setMaxResults(static::PAGINATION_LIMIT);

        switch ($sortBy) {
            case 'created_at':
                $query->orderBy('link.createdAt', $sortDirection);
                break;
            case 'url':
                $query->orderBy('link.url', $sortDirection);
                break;
            case 'title':
                $query->orderBy('link.title', $sortDirection);
                break;
            default:
                $query->orderBy('link.id', $sortDirection);
                break;
        }

        $paginator = new Paginator($query);

        $items = $paginator->getQuery()->getResult();
        $totalElements = count($paginator);
        $totalPages = ceil($totalElements / static::PAGINATION_LIMIT);

        return new PaginatedData($totalElements, $totalPages, $page, $items);
    }
}
