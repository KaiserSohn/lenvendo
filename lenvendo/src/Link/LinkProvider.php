<?php

declare(strict_types=1);

namespace App\Link;

use App\Exception\LinkProviderException;
use App\Link\Dto\ParsedLinkData;
use Exception;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\ContentLengthException;
use PHPHtmlParser\Exceptions\LogicalException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

class LinkProvider
{
    private const HTTP_METHOD = 'GET';

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * LinkProvider constructor.
     * @param HttpClientInterface $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $link
     * @throws LinkProviderException
     */
    private function getRequestResult(string $link)
    {
        try {
            $request = $this->httpClient->request(
                static::HTTP_METHOD,
                $link,
                ['timeout' => 10]
            );

            if ($request->getStatusCode() !== Response::HTTP_OK) {
                throw new Exception('Данный сервис не доступен');
            }

            return $request->getContent();
        } catch (Throwable $exception) {
            throw new LinkProviderException($exception->getMessage());
        }
    }

    /**
     * @param string $url
     * @return ParsedLinkData
     * @throws LinkProviderException
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws NotLoadedException
     * @throws StrictException
     */
    public function getParsedData(string $url): ParsedLinkData
    {
        $request = $this->getRequestResult($url);
        $title = null;
        $favicon = null;
        $metaDescription = null;
        $metaKeywords = null;


        $dom = new Dom();
        $dom->loadStr($request);

        /** @var Dom\Node\TextNode $titleSelector */
        $titleSelector = $dom->find('meta[property="og:title"]')[0];
        $title = $titleSelector ? $titleSelector->getAttribute('content') : null;

        if (!$title) {
            $titleSelector = $dom->find('title')[0];
            $title = $titleSelector->innerHtml();
        }

        /** @var Dom\Node\TextNode $faviconSelector */
        $faviconSelector = $dom->find('link[rel="shortcut icon"]')[0];
        $favicon = $faviconSelector ? $faviconSelector->getAttribute('href') : null;

        /** @var Dom\Node\TextNode $metaDescriptionSelector */
        $metaDescriptionSelector = $dom->find('meta[property="og:description"]')[0];
        $metaDescription = $metaDescriptionSelector ? $metaDescriptionSelector->getAttribute('content') : null;

        if (!$metaDescription) {
            $metaDescriptionSelector = $dom->find('meta[name="description"]')[0];
            $metaDescription = $metaDescriptionSelector ? $metaDescriptionSelector->getAttribute('content') : null;
        }

        /** @var Dom\Node\TextNode $metaKeywordsSelector */
        $metaKeywordsSelector = $dom->find('meta[name="keywords"]')[0];
        $metaKeywords = $metaKeywordsSelector ? $metaKeywordsSelector->getAttribute('content') : null;

        return new ParsedLinkData($title, $favicon, $metaDescription, $metaKeywords);
    }
}
